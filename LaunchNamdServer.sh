#!/bin/bash
# Para ejecutar el script estando desde yoda:
# ssh -t nombredelservidoraconectarse bash /rutaalscript/LaunchNamdServer.sh

#CGalaz 04-06-20 (In corona times) v. alpha-lab 1.0
#CGalaz 06-06-20 v. alpha-lab 2.0
#CGalaz 09-06-20 v. alpha-lab 3.0
#CGalaz 14-06-20 v. alpha-lab 4.0
#CGalaz 02-07-20 v. alpha-lab 5.0
#CGalaz 14-07-20 v. alpha-lab 6.0

showMenu () {
	echo " --------------------------------------------------- "
	echo " -----           Welcome to NAMD launcher      ----- "
	echo " --------------------------------------------------- "
	echo " --                    MENU                       -- "
	echo " --[1] Ver uso de las CPUs                        -- "
	echo " --[2] Ver CPUs disponibles en el servidor        -- "
	echo " --[3] Ver uso de las GPUs                        -- "
	echo " --[4] Ver GPUs disponibles en el servidor        -- "
	echo " --[5] Lanzar calculo de NAMD para su sistema     -- "
	echo " --[6] Ver configuracion de calculos andando      -- "
	echo " --     en el servidor                            -- "
	echo " --[7] Salir                                      -- "
	echo " --------------------------------------------------- "
	echo " -----            (v. alpha-lab 6.0)           ----- "
	echo " --------------------------------------------------- "
}

cpuSeeUseOption1 () {
	echo " --------------------------------------------------- "
    echo " -----            Ver uso de las CPUs          ----- "
    echo " --------------------------------------------------- "
    #Para extaer los porcentajes de uso (para mas detalles, leer el comando mpstat)
    mpstat -P ALL 1 1 > cpu_use_1.txt
    #Eliminamos las primeras lineas, que consisten en la descripcion de nuestro equipo y numero de CPUs
    tail -n +5 cpu_use_1.txt > cpu_use_2.txt
    #Nos quedamos con el porcentaje de uso de usuario (cuando se deja andando un calculo de simulacion,
    #este aumenta abruptamente)
    #Para el servidor
    sed '/Average/d' cpu_use_2.txt > cpu_use_3.txt
    #Esto aparece en c3p0
    #sed '/Media/d' cpu_use_2.txt > cpu_use_3.txt
    #Para el servidor
    cpu_usage=( $(awk '{ print $4 }' cpu_use_3.txt) )
    #cpu_usage=( $(awk '{ print $3 }' cpu_use_3.txt) )
    #Los imprime en base al ID de la CPU y el porcentaje de uso
    for ((i=0; i < ${#cpu_usage[@]}; i++))
    do
        echo -e ' \t ' " ----- P["$i"] Uso: "${cpu_usage[$i]} " ----- "
    done
    rm *.txt

}

freeCpuSeeOption2 () {
	#La idea es que en los servidores esto sea un array, asi en la opcion 4 se despliegan
	#los IDs de las gpu libres
	free_cpu=0
	echo " --------------------------------------------------- "
	echo " -----   Ver CPUs disponibles en el servidor   ----- "
	echo " --------------------------------------------------- "
	#Utilizamos lo mismo de la opcion 1, solo que ahora vamos a comparar
	mpstat -P ALL 1 1 > cpu_use_1.txt
	tail -n +5 cpu_use_1.txt > cpu_use_2.txt
	#Para el servidor
    sed '/Average/d' cpu_use_2.txt > cpu_use_3.txt
	#sed '/Media/d' cpu_use_2.txt > cpu_use_3.txt
	sed 's/,/./g' cpu_use_3.txt > cpu_use_4.txt
	#Para el servidor
    cpu_usage=( $(awk '{ print $4 }' cpu_use_4.txt) )
	#cpu_usage=( $(awk '{ print $3 }' cpu_use_4.txt) )
	for ((i=0; i < ${#cpu_usage[@]}; i++))
	do
		#REVISAR CUAL SERÁ EL CRITERIO PARA DECIR QUE LA CPU ESTA EN USO (MENOR AL 15%)
	    if [ $(echo "${cpu_usage[$i]}>20"| bc) -eq 0 ];
			then
				echo -e ' \t ' " ----- P["$i"] Uso: "${cpu_usage[$i]} " ----- "
				free_cpu=$((free_cpu+1))
		fi
	done
	if [ $free_cpu -eq 0 ];
		then
			echo " -----                   NO HAY                ----- "
			echo " --------------------------------------------------- "
	fi

	if [ $free_cpu -gt 0 ];
		then
			echo " --------------------------------------------------- "
			echo " -----           CPUs LIBRES: "$free_cpu"               ----- "
			echo " --------------------------------------------------- "
	fi

	rm *.txt
}

gpuSeeUseOption3 () {
	free_gpu=0
	echo " --------------------------------------------------- "
	echo " -----           Ver uso de las GPUs           ----- "
	echo " --------------------------------------------------- "
	nvidia-smi > gpu_use_1.txt
	#Para obtener la cantidad de GPUs que tiene el equipo
	grep -e Off gpu_use_1.txt > gpu_use_2.txt 
    gpu_amount=( $(awk '{ print $2 }' gpu_use_2.txt) )

	tail -n +16 gpu_use_1.txt > gpu_use_3.txt
	grep -e namd2 gpu_use_3.txt > gpu_use_4.txt
	gpu_usage=( $(awk '{ print $2 }' gpu_use_4.txt) )
	len_used=${gpu_usage[@]}

	if [ -z "$len_used" ]; 
	then
		for ((i=0; i < ${#gpu_amount[@]}; i++))
		do
			echo -e ' \t\t' " GPU["${gpu_amount[$i]}"] LIBRE "
			free_gpu=$((free_gpu+1))
		done 
	else 
		for ((i=0; i < ${#gpu_amount[@]}; i++))
		do
			gpu_is_used=0
			for ((j=0; j < ${#gpu_usage[@]}; j++))
			do
				if [ ${gpu_amount[$i]} -eq ${gpu_usage[$j]} ] && [ $gpu_is_used -eq 0 ];
				then
					echo -e ' \t\t' " GPU["${gpu_amount[$i]}"] en USO"
					gpu_is_used=1
				fi
			done

			if [ $gpu_is_used -eq 0 ];
			then
				echo -e ' \t\t' " GPU["${gpu_amount[$i]}"] LIBRE "
				free_gpu=$((free_gpu+1))
			fi
		done
	fi

	echo -e ' \t' " Cantidad de GPUs libres: "$free_gpu
	rm *.txt
}

freeGpuSeeOption4 (){
	free_gpu=0
	echo " --------------------------------------------------- "
	echo " -----   Ver GPUs disponibles en el servidor   ----- "
	echo " --------------------------------------------------- "
	nvidia-smi > gpu_use_1.txt
	#Para obtener la cantidad de GPUs que tiene el equipo
	grep -e Off gpu_use_1.txt > gpu_use_2.txt 
    gpu_amount=( $(awk '{ print $2 }' gpu_use_2.txt) )

	tail -n +16 gpu_use_1.txt > gpu_use_3.txt
	grep -e namd2 gpu_use_3.txt > gpu_use_4.txt
	gpu_usage=( $(awk '{ print $2 }' gpu_use_4.txt) )
	len_used=${gpu_usage[@]}

	if [ -z "$len_used" ]; 
	then
		for ((i=0; i < ${#gpu_amount[@]}; i++))
		do
			echo -e ' \t\t' " GPU["${gpu_amount[$i]}"] LIBRE "
			free_gpu=$((free_gpu+1))
		done 
	else 
		for ((i=0; i < ${#gpu_amount[@]}; i++))
		do
			gpu_is_used=0
			for ((j=0; j < ${#gpu_usage[@]}; j++))
			do
				if [ ${gpu_amount[$i]} -eq ${gpu_usage[$j]} ] && [ $gpu_is_used -eq 0 ];
				then
					gpu_is_used=1
				fi
			done
			
			if [ $gpu_is_used -eq 0 ];
			then
				echo -e ' \t\t' " GPU["${gpu_amount[$i]}"] LIBRE "
				free_gpu=$((free_gpu+1))
			fi
		done
	fi

	echo -e ' \t' " Cantidad de GPUs libres: "$free_gpu
	rm *.txt
}

launchNamdSimulationOption5 (){
	echo " --------------------------------------------------- "
	echo " ----- Lanzar calculo de NAMD para su sistema  ----- "
	echo " --------------------------------------------------- "
	free_cpu=0
	echo " --------------------------------------------------- "
	echo " -----   CPUs disponibles en el servidor    ----- "
	echo " --------------------------------------------------- "
	#Utilizamos lo mismo de la opcion 1, solo que ahora vamos a comparar
	mpstat -P ALL 1 1 > cpu_use_1.txt
	tail -n +5 cpu_use_1.txt > cpu_use_2.txt
	#Para el servidor
    sed '/Average/d' cpu_use_2.txt > cpu_use_3.txt
	#sed '/Media/d' cpu_use_2.txt > cpu_use_3.txt
	sed 's/,/./g' cpu_use_3.txt > cpu_use_4.txt
	#Para el servidor
    cpu_usage=( $(awk '{ print $4 }' cpu_use_4.txt) )
    #cpu_usage=( $(awk '{ print $3 }' cpu_use_4.txt) )
	for ((i=0; i < ${#cpu_usage[@]}; i++))
	do
		#REVISAR CUAL SERÁ EL CRITERIO PARA DECIR QUE LA CPU ESTA EN USO (MENOR AL 15%)
	    if [ $(echo "${cpu_usage[$i]}>20"| bc) -eq 0 ];
			then
				echo -e ' \t ' " ----- P["$i"] Uso: "${cpu_usage[$i]} " ----- "
				free_cpu=$((free_cpu+1))
		fi
	done
	if [ $free_cpu -eq 0 ];
		then
			echo " -----                   NO HAY                ----- "
			echo " --------------------------------------------------- "
	fi

	if [ $free_cpu -gt 0 ];
		then
			echo " --------------------------------------------------- "
			echo " -----           CPUs LIBRES: "$free_cpu"               ----- "
			echo " --------------------------------------------------- "
	fi
	
	echo " --------------------------------------------------- "
	echo " -----       GPUs disponibles en el servidor   ----- "
	echo " --------------------------------------------------- "
	free_gpu=0
	nvidia-smi > gpu_use_1.txt
	#Para obtener la cantidad de GPUs que tiene el equipo
	grep -e Off gpu_use_1.txt > gpu_use_2.txt 
    gpu_amount=( $(awk '{ print $2 }' gpu_use_2.txt) )

	tail -n +16 gpu_use_1.txt > gpu_use_3.txt
	grep -e namd2 gpu_use_3.txt > gpu_use_4.txt
	gpu_usage=( $(awk '{ print $2 }' gpu_use_4.txt) )
	len_used=${gpu_usage[@]}
	if [ -z "$len_used" ]; 
	then
		for ((i=0; i < ${#gpu_amount[@]}; i++))
		do
			#Las IDs de las GPUs libres se guardan en un nuevo array
			echo -e ' \t\t' " GPU["${gpu_amount[$i]}"] LIBRE "
			gpu_can_be_used[$free_gpu]=${gpu_amount[$i]};
			free_gpu=$((free_gpu+1))
		done 
	else 
		for ((i=0; i < ${#gpu_amount[@]}; i++))
		do
			gpu_is_used=0
			for ((j=0; j < ${#gpu_usage[@]}; j++))
			do
				if [ ${gpu_amount[$i]} -eq ${gpu_usage[$j]} ] && [ $gpu_is_used -eq 0 ];
				then
					gpu_is_used=1
				fi
			done
			
			if [ $gpu_is_used -eq 0 ];
			then
				echo -e ' \t\t' " GPU["${gpu_amount[$i]}"] LIBRE "
				gpu_can_be_used[$free_gpu]=${gpu_amount[$i]};
				free_gpu=$((free_gpu+1))
			fi
		done
	fi

	if [ $free_gpu -eq 0 ];
	then
		echo " -----                   NO HAY                ----- "
		echo " --------------------------------------------------- "
	fi

	#El programa pregunta y se dedica a buscar las CPUs libres
	#en base a las IDs

	if [ $free_cpu -ne 0 ] && [ $free_gpu -ne 0 ] ;
	then
		echo "HAY RECURSOS DISPONIBLES"
		echo " ------------------------------------------------------ "
		echo " Para poder lanzar el calculo debe ingresar la cantidad "
		echo " de CPUs a utilizar: "
		echo " Ej. 6"
		echo "Ingrese un numero mayor que cero:"
		read amount_cpu
		while ! [ -z "${amount_cpu##[0-9]*}" ] || [ -z "$amount_cpu" ] || [ $amount_cpu -eq 0 ] ;
		do 
			echo "Ingrese un numero mayor que cero:"
			read amount_cpu
		done 
		echo " "
		echo " Para poder lanzar el calculo debe ingresar la cantidad"
		echo " de GPUs a utilizar: "
		echo " Ej. 1"
		echo "Ingrese un numero mayor que cero:"
		read amount_gpu
		while ! [ -z "${amount_gpu##[0-9]*}" ] || [ -z "$amount_gpu" ] || [ $amount_gpu -eq 0 ];
		do 
			echo "Ingrese un numero mayor que cero:"
			read amount_gpu
		done 
		echo " "
		echo " Para poder lanzar el calculo debe ingresar el nombre de"
		echo " su archivo de configuracion NAMD o directorio:"
		echo " Ej. Nombre_archivo_namd (sin el .namd)"
		echo " En caso de indicar directorio:"
		echo " Ej. /home/usuario/nombre_ruta/Nombre_archivo_namd (sin el .namd)"
		read namd_file
		while [ -z "$namd_file" ] ;
		do 
			echo "Ingrese el nombre del archivo:"
			read namd_file
		done 
		echo " "

		namd2_command="/share/apps/namd/2.13/NAMD_2.13_Linux-x86_64-multicore-CUDA/namd2 +p $amount_cpu +setcpuaffinity +pemap "
		j=1

		for ((i=0; i < ${#cpu_usage[@]}; i++))
		do
			#REVISAR CUAL SERÁ EL CRITERIO PARA DECIR QUE LA CPU ESTA EN USO (MENOR AL 15%)
		    if [ $j -lt $amount_cpu ] && [ $(echo "${cpu_usage[$i]}>20"| bc) -eq 0 ];
				then
					namd2_command+="$i,"
					j=$((j+1))
			elif [ $j -eq $amount_cpu ] && [ $(echo "${cpu_usage[$i]}>20"| bc) -eq 0 ];
				then
				namd2_command+="$i "
				j=$((j+1))
			fi
		done

		namd2_command+="+isomalloc_sync +devices "
		#Nombre de archivo namd de prueba: abf1D_agWat_Trp.w1.1

		if [ $amount_cpu -le $free_cpu ] && [ $amount_gpu -le $free_gpu ];
		then
			j=1
			for ((i=0; i < ${#gpu_can_be_used[@]}; i++))
			do
				if [ $j -lt $amount_gpu ];
				then
					namd2_command+=""${gpu_can_be_used[$i]}","
					j=$((j+1))
				elif [ $j -eq $amount_gpu ];
					then
					namd2_command+=${gpu_can_be_used[$i]}" "
					j=$((j+1))
				fi
			done

			namd2_command+=""$namd_file".namd"

			clear
			echo "Instrucccion construida: "$namd2_command
			nohup $namd2_command > $namd_file".log" &
		else 
			clear
			echo "- ERROR -"
			#echo "Instruccion construida antes del error: "$namd2_command
			echo "CPUs solicitadas :"$amount_cpu" - CPUs libres: "$free_cpu
			echo "GPUs solicitadas :"$amount_gpu" - GPUs libres: "$free_gpu
			echo "Nombre de archivo NAMD ingresado: "$namd_file
			echo "En caso de que si estaban los recursos correspondientes "
			echo "ocurre que escribio de manera erronea el archivo namd o "
			echo "no especifico correctamente el directorio"
		fi
	else 
		echo " NO HAY RECURSOS, NO PUEDE LANZAR CALCULOS AHORA"
		echo " VUELVA EN OTRO MOMENTO"
	fi

	rm *.txt
}

#Por el momento por que no se obtienen los mismos resultados como el proceso que esta
#corriendo el leia, es por que se esta usando otra linea para correr namd
#EJ. de lo que estaba corriendo en leia:
#hpoblete 230595      1 99 Jun02 pts/0    69-20:54:10 /usr/local/NAMD_2.13_Linux-x86_64-multicore-CUDA/namd2 +idlepoll +isomalloc_sync +devices 0 +p16 MbKDEK2020produccion.conf

seeNamdRunningOption7 () {
		echo " --------------------------------------------------- "
	echo " -----         CORRIENDO EN SERVER             ----- "
	echo " --------------------------------------------------- "

	#Para extraer los procesos namd2 que se esten corriendo en el equipo
	#sin importar que usuario lo esta corriendo
	ps -fC 'namd2' > namd_running_on_server_1.txt
	tail -n +2 namd_running_on_server_1.txt > namd_running_on_server_2.txt

	#Para saber el usuario
	user_running_namd=( $(awk '{ print $1 }' namd_running_on_server_2.txt) )

	#Para saber la cantidad de CPUs que esta usando
	cpu_running_namd=( $(awk '{ print $10 }' namd_running_on_server_2.txt) )

	#Para saber cuales CPUs esta usando
	idcpu_running_namd=( $(awk '{ print $13 }' namd_running_on_server_2.txt) )

	#Para saber cuales GPUs esta usando
	idgpu_running_namd=( $(awk '{ print $16 }' namd_running_on_server_2.txt) )

	echo "Corriendo con la configuracion de este script"
	echo " "
	for ((i=0; i < ${#user_running_namd[@]}; i++))
	do
		echo "El usuario: "${user_running_namd[$i]}""
		echo "esta usando: "${cpu_running_namd[$i]}" CPU(s)"
		echo "que corresponde(n) a la(s) que tiene(n) Id: "${idcpu_running_namd[$i]}
		echo "con la(s) siguiente(s) GPU(s): "${idgpu_running_namd[$i]}
		echo " --------------------------------------------------- "
	done

	len_namd_used=${user_running_namd[@]}

	if [ $i -eq 0 ];
	then
		echo ""
		echo "            SEVIDOR LIBRE POR EL MOMENTO "
		echo ""
	fi

	rm *.txt
}

mainMenu () {
	clear

	option=1

	while [ $option -lt 8 ] && [ $option -gt 0 ] && [ $option -ne 7 ]; do 
		showMenu
		echo " Ingrese una opcion: "
		read option
		if ! [ -z "$option" ];
		then
			if [ -z "${option##[0-9]*}" ];
			then 
				clear
				if [ $option -eq 1 ]
				then
					cpuSeeUseOption1 
				elif [ $option -eq 2 ]
				then
					freeCpuSeeOption2
				elif [ $option -eq 3 ]
				then
					gpuSeeUseOption3
				elif [ $option -eq 4 ]
				then
					freeGpuSeeOption4
				elif [ $option -eq 5 ]
				then
					launchNamdSimulationOption5
				elif [ $option -eq 6 ]
				then
					seeNamdRunningOption7
				elif [ $option -eq 7 ]
				then
					echo " --------------------------------------------------- "
					echo " -----       HA CERRADO EL PROGRAMA            ----- "
					echo " --------------------------------------------------- "
				else 
					echo " --------------------------------------------------- "
					echo " -----           ERROR DE OPCION               ----- "
					echo " --------------------------------------------------- "
					echo " DEBE SER UN NUMERO DE LOS QUE SE INDICA EN EL MENU"
					echo " DE OPCIONES"
					option=1
				fi
			else 
				clear
				echo " --------------------------------------------------- "
				echo " -----           ERROR DE OPCION               ----- "
				echo " --------------------------------------------------- "
				echo " NO HA INGRESADO UN NÚMERO"
				echo " VUELVA A INGRESAR UNA OPCION DEL MENU"
				option=1
			fi 
		else
			clear
			echo " --------------------------------------------------- "
			echo " -----           ERROR DE OPCION               ----- "
			echo " --------------------------------------------------- "
			echo " NO HA INGRESADO UN NÚMERO"
			echo " VUELVA A INGRESAR UNA OPCION DEL MENU"
			option=1
		fi
	done
}

if [ $# -ne 0 ] 
then
	echo "El uso es LaunchNamdServer.sh de la siguiente forma:"
	echo "ssh -t nombredelservidoraconectarse bash LaunchNamdServer.sh"
	echo "Recuerde que debe estar desde el servidor Yoda para ejecutarlo"
	echo "Si está en el servidor que desea lanzar simulaciones"
	echo "Solo debe utilizar: LaunchNamdServer.sh"
else
	mainMenu
fi